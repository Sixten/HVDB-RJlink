// ==UserScript==
// @name         Link to DLsite in HVDB WorkDetails
// @namespace    Sixten
// @version      1.0.4
// @description  Replaces the RJ part of the "Work Details" text node on the WorkDetails subpages of HVDB with a link to the content on DLsite
// @author       Sixten
// @match        http://hvdb.me/Dashboard/WorkDetails/*
// @match        https://hvdb.me/Dashboard/WorkDetails/*
// @match        http://hvdb.me/Dashboard/Details/*
// @match        https://hvdb.me/Dashboard/Details/*
// @downloadURL  https://gitlab.com/Sixten/HVDB-RJlink/raw/master/HVDB-RJlinks.user.js
// @updateURL    https://gitlab.com/Sixten/HVDB-RJlink/raw/master/HVDB-RJlinks.user.js
// @grant        none
// @run-at       document-end
// @source       https://gitlab.com/Sixten/HVDB-RJlink/
// ==/UserScript==

(function() {
    'use strict';
    let rjRegex = new RegExp("R[JE][0-9]{6}", "gi"); //stolen, I'm sorry
    let element = document.getElementsByClassName('container-fluid body-content').item(0).getElementsByTagName('h2').item(0);
    if(!element) return;
    let rjCode = element.textContent.match(rjRegex)[0];
    element.textContent = element.textContent.replace(rjCode, '');
    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', 'http://www.dlsite.com/maniax/work/=/product_id/'+rjCode+'.html');
    linkElement.setAttribute('target', '_blank');
    linkElement.textContent = rjCode;
    element.appendChild(linkElement);
})();